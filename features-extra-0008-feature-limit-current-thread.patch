[PATCH] feature: limit-current-thread

Add command to limit view on the thread from any of its message.
Predefined on command <ESC>L, though the lowercase 'l' would be better
(less keystrokes), but it's already occupied by a not-so-useful command
"show limit".

Tested on large folders, no performance problems observed.

Search in current thread is not implemented.

Signed-off-by: David Sterba <dsterba@suse.cz>

diff -r ef40b0a13e23 -r 031fb0d6255d OPS
--- a/OPS	Sat Jan 30 16:27:19 2016 +0000
+++ b/OPS	Thu May 24 15:34:53 2012 +0200
@@ -1184,6 +1184,11 @@ OP_WHAT_KEY N_("display the keycode for 
  */
 OP_CHECK_STATS N_("calculate message statistics for all mailboxes")
 
+/* L10N: Help screen description for OP_LIMIT_CURRENT_THREAD
+   index menu: <limit-current-thread>
+ */
+OP_LIMIT_CURRENT_THREAD N_("limit view to current thread")
+
 /* L10N: Help screen description for OP_MAIN_SHOW_LIMIT
    index menu: <show-limit>
  */
diff -r ef40b0a13e23 -r 031fb0d6255d PATCHES
--- a/PATCHES	Sat Jan 30 16:27:19 2016 +0000
+++ b/PATCHES	Thu May 24 15:34:53 2012 +0200
@@ -1,3 +1,4 @@
+patch-limit-current-thread-gentoo         David Sterba <dsterba@suse.cz>
 patch-quasi-delete-gentoo                 Karel Zak <kzak@redhat.com>
 patch-status-color-gentoo                 David Sterba <dsterba@suse.cz>
 patch-progress-gentoo                     Rocco Rutte <pdmef@gmx.net>
diff -r ef40b0a13e23 -r 031fb0d6255d curs_main.c
--- a/curs_main.c	Sat Jan 30 16:27:19 2016 +0000
+++ b/curs_main.c	Thu May 24 15:34:53 2012 +0200
@@ -1171,12 +1171,17 @@ int mutt_index_menu (void)
 	}
         break;
 
+      case OP_LIMIT_CURRENT_THREAD:
       case OP_MAIN_LIMIT:
 
 	CHECK_IN_MAILBOX;
 	menu->oldcurrent = (Context->vcount && menu->current >= 0 && menu->current < Context->vcount) ?
           CURHDR->index : -1;
-	if (mutt_pattern_func (MUTT_LIMIT, _("Limit to messages matching: ")) == 0)
+	if (((op == OP_LIMIT_CURRENT_THREAD) &&
+	      mutt_limit_current_thread(CURHDR)) ||
+	    ((op == OP_MAIN_LIMIT) &&
+	     (mutt_pattern_func (MUTT_LIMIT,
+				 _("Limit to messages matching: ")) == 0)))
 	{
 	  if (menu->oldcurrent >= 0)
 	  {
diff -r ef40b0a13e23 -r 031fb0d6255d functions.h
--- a/functions.h	Sat Jan 30 16:27:19 2016 +0000
+++ b/functions.h	Thu May 24 15:34:53 2012 +0200
@@ -179,6 +179,7 @@ const struct menu_func_op_t OpMain[] = {
   { "imap-logout-all",           OP_MAIN_IMAP_LOGOUT_ALL },
 #endif
   { "limit",                     OP_MAIN_LIMIT },
+  { "limit-current-thread",      OP_LIMIT_CURRENT_THREAD },
   { "link-threads",              OP_MAIN_LINK_THREADS },
   { "list-action",               OP_LIST_ACTION },
   { "list-reply",                OP_LIST_REPLY },
diff -r ef40b0a13e23 -r 031fb0d6255d mutt_menu.h
--- a/mutt_menu.h	Sat Jan 30 16:27:19 2016 +0000
+++ b/mutt_menu.h	Thu May 24 15:34:53 2012 +0200
@@ -143,4 +143,6 @@ int mutt_menuLoop (MUTTMENU *);
 void index_make_entry (char *, size_t, struct menu_t *, int);
 COLOR_ATTR index_color (int);
 
+int mutt_limit_current_thread (HEADER *h);
+
 #endif /* _MUTT_MENU_H_ */
diff -r ef40b0a13e23 -r 031fb0d6255d pattern.c
--- a/pattern.c	Sat Jan 30 16:27:19 2016 +0000
+++ b/pattern.c	Thu May 24 15:34:53 2012 +0200
@@ -1868,6 +1868,74 @@ void mutt_check_simple (BUFFER *s, const
   }
 }
 
+/**
+ * top_of_thread - Find the first email in the current thread
+ * @h: Header of current email
+ *
+ * Returns:
+ *	THREAD*: success, email found
+ *	NULL:    on error
+ */
+static THREAD *
+top_of_thread (HEADER *h)
+{
+  THREAD *t;
+
+  if (!h)
+    return NULL;
+
+  t = h->thread;
+
+  while (t && t->parent)
+    t = t->parent;
+
+  return t;
+}
+
+/**
+ * mutt_limit_current_thread - Limit the email view to the current thread
+ * @h: Header of current email
+ *
+ * Returns:
+ *	1: Success
+ *	0: Failure
+ */
+int
+mutt_limit_current_thread (HEADER *h)
+{
+  int i;
+  THREAD *me;
+
+  if (!h)
+    return 0;
+
+  me = top_of_thread (h);
+  if (!me)
+    return 0;
+
+  Context->vcount    = 0;
+  Context->vsize     = 0;
+  Context->collapsed = 0;
+
+  for (i = 0; i < Context->msgcount; i++) {
+    Context->hdrs[i]->virtual    = -1;
+    Context->hdrs[i]->limited    = 0;
+    Context->hdrs[i]->collapsed  = 0;
+    Context->hdrs[i]->num_hidden = 0;
+
+    if (top_of_thread (Context->hdrs[i]) == me) {
+      BODY *body = Context->hdrs[i]->content;
+
+      Context->hdrs[i]->virtual = Context->vcount;
+      Context->hdrs[i]->limited = 1;
+      Context->v2r[Context->vcount] = i;
+      Context->vcount++;
+      Context->vsize += (body->length + body->offset - body->hdr_offset);
+    }
+  }
+  return 1;
+}
+
 int mutt_pattern_func (int op, char *prompt)
 {
   pattern_t *pat = NULL;
